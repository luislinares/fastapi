# Python
from typing import Optional
from enum import Enum

# Pydantic
from pydantic import BaseModel, Field, EmailStr

# fastapi
from fastapi import FastAPI, status, HTTPException
from fastapi import Path, Query, Body, Form, Header, Cookie, UploadFile, File

app = FastAPI()


# Models
class HairColor(Enum):
    white = "white"
    brown = "brown"
    black = "black"
    blonde = "blonde"
    red = "red"


class Location(BaseModel):
    city: str
    state: str
    country: str


class PersonBase(BaseModel):
    first_name: str = Field(
        ...,
        min_length=1,
        max_length=50,
        example="Luis"
        )
    last_name: str = Field(
        ...,
        min_length=1,
        max_length=50,
        example="Linares"
        )
    age: int = Field(
        ...,
        gt=0,
        le=115,
        example="31"
    )
    hair_color: Optional[HairColor] = Field(default=None, example="black")
    is_married: Optional[bool] = Field(default=None, example=False)   


class Person(PersonBase):
    password: str = Field(..., min_length=8)
    
    # class Config:
    #     schema_extra = {
    #         "example" : {
    #             "first_name": "Luis",
    #             "last_name": "Linares",
    #             "age": "31",
    #             "hair_color": "black",
    #             "is_married": False
    #         }
    #     }


class PersonOut(PersonBase):
    pass


class LoginOut(BaseModel):
    username: str = Field(..., max_length=20, example="luis10")
    message: str = Field(default="Login Successfully! ")


@app.get(
    path="/", 
    status_code=status.HTTP_200_OK
    )
def home():
    return {"hello": "work"}


# request and response body
@app.post(
    path="/person/new", 
    response_model=PersonOut,
    status_code=status.HTTP_201_CREATED,
    tags=["Persons"],
    summary="Create person in the app"
    )
def create_person(person: Person = Body(...)):
    """
    Create person

    This path operation create a person in the app and save the information in the database

    Parameters:
    - Request body parameter:
        - **person: Person** -> person model with first name, last name, age, hair color and marital status

    Return a person model with first name, last name, age, hair color and marital status
    """
    return person    


# validation query param
@app.get(
    path="/person/detail",
    status_code=status.HTTP_200_OK,
    tags=["Persons"]
    )
def show_person(
    name: Optional[str] = Query(
        None, 
        min_length=1, 
        max_length=50,
        title="Person Name",
        description="This is the person name. It's between 1 and 50 characters",
        example="Rocio"
        ),
    age: str = Query(
        ...,
        title="Person Age",
        description="This is the person age, It's required",
        example="25"
        )
):   
    return {name: age}


# validation path param
persons = [1, 2, 3, 4, 5]


@app.get(
    path="/person/detail/{person_id}",
    status_code=status.HTTP_200_OK,
    tags=["Persons"]
)
def show_person(
    person_id: int = Path(
        ..., 
        gt=0,
        example=123
        )
):
    if person_id not in persons:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail="This person doesn't exist"
        )
    return {person_id: "It exists!!"}   


# forms
@app.post(
    path="/login",
    response_model=LoginOut,
    status_code=status.HTTP_200_OK,
    tags=["Persons"]
)
def login(
        username: str = Form(...),
        password: str = Form(...)
):
    return LoginOut(username=username)


# validation request body
@app.put(
    path="/person/{person_id}",
    status_code=status.HTTP_200_OK,
    tags=["Persons"]
)
def update_person(
    person_id: int = Path(
        ...,
        title="Person ID",
        description="This is the person ID",
        gt=0,
        example=123
    ),
    person: Person = Body(...),
    # location: Location = Body(...)
):
    # results = person.dict()
    # results.update(location.dict())
    # return results
    return person


# cookies and headers parameters
@app.post(
    path="/contact",
    status_code=status.HTTP_200_OK
)
def contact(
        first_name: str = Form(
            ...,
            max_length=20,
            min_length=1
        ),
        last_name: str = Form(
            ...,
            max_length=20,
            min_length=1
        ),
        email: EmailStr = Form(...),
        message: str = Form(
            ...,
            min_length=20
        ),
        user_agent: Optional[str] = Header(default=None),
        ads: Optional[str] = Cookie(default=None)
):
    return user_agent


# files
@app.post(
    path="/post-image"
)
def post_image(
        image: UploadFile = File(...)
):
    return {
        "Filename": image.filename,
        "Format": image.content_type,
        "Size(kb)": round(len(image.file.read())/1024, ndigits=2)
    }
